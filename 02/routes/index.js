const router = require("express").Router();

const Joi = require("joi");
const footballController = require("../controllers/football.controller");
const containLetterController = require("../controllers/contain_letter.controller");
const { inputValidation } = require("../middlewares/validation");

/* GET home page. */

router.get("/", (req, res, next) => {
  res.render("index", { title: "Express" });
});

/* API ROUTE */

router.post(
  "/football/recordgame",
  inputValidation({
    body: Joi.object({
      clubhomename: Joi.string().required(),
      clubawayname: Joi.string().required(),
      score: Joi.string()
        .pattern(new RegExp(/^[0-9]+\s:\s[0-9]+$/))
        .required(),
    }),
  }),
  footballController.postRecordGame
);

router.get("/football/leaguestanding", footballController.getLeagueStanding);

router.get(
  "/football/rank",
  inputValidation({
    query: Joi.object({
      clubname: Joi.string().required(),
    }),
  }),
  footballController.getRankByQuery
);

router.post(
  "/is-contain-letters",
  inputValidation({
    body: Joi.object({
      first_word: Joi.string().required(),
      second_word: Joi.string().required(),
    }),
  }),
  containLetterController.isContainLetter
);

module.exports = router;
