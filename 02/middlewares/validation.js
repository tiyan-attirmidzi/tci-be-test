const inputValidation = (schema) => {
  return (req, res, next) => {
    const keys = Object.keys(schema);

    for (let i = 0; i < keys.length; i++) {
      const { error } = schema[keys[i]].validate(req[keys[i]]);
      const valid = error == null;
      if (!valid) {
        const { details } = error;
        const message = details.map((i) => i.message).join(",");
        console.log("error", message);
        res.status(422).json({
          message: "Invalid Data",
          error: message,
        });
      }
    }

    next();
  };
};

module.exports = {
  inputValidation,
};
