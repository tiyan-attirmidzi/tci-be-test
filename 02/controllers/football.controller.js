let lists = [];

function add(club) {
  var found = false;
  for (const key in lists) {
    if (lists[key].clubname == club.clubname) {
      lists[key].points += club.points;
      found = true;
      break;
    }
  }
  if (!found) {
    lists.push(club);
  }
  lists.sort((a, b) => b.points - a.points);
}

function search(keyword, standings) {
  return standings
    .map((e, i) => {
      e.standing = i + 1;
      delete e.points;
      return e;
    })
    .filter((e) => e.clubname.includes(keyword));
}

const postRecordGame = (req, res, next) => {
  const { clubhomename, clubawayname, score } = req.body;

  const scores = score.split(" : ");

  add({
    clubname: clubhomename,
    points: scores[0] > scores[1] ? 3 : scores[0] === scores[1] ? 1 : 0,
  });

  add({
    clubname: clubawayname,
    points: scores[0] < scores[1] ? 3 : scores[0] === scores[1] ? 1 : 0,
  });

  res.status(201).json({
    message: "Record Game Stored Successfully!",
  });
};

const getLeagueStanding = (req, res, next) => {
  res.status(200).json({
    message: "League Standing Retrieved Successfully!",
    data: lists,
  });
};

const getRankByQuery = (req, res, next) => {
  const { clubname } = req.query;

  var temp = JSON.parse(JSON.stringify(lists));

  const result = search(clubname, temp);

  res.status(200).json({
    message: "Rank Retrieved Successfully!",
    data: result,
  });
};

module.exports = {
  postRecordGame,
  getLeagueStanding,
  getRankByQuery,
};
