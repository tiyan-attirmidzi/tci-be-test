const isContainLetter = (req, res, next) => {
  const { first_word, second_word } = req.body;
  let couter = 0;

  const arrFirstWord = [...new Set([...first_word])].map((e) => {
    return e.toLowerCase();
  });

  const arrSecondWord = [...second_word].map((e) => {
    return e.toLowerCase();
  });

  for (let i = 0; i < arrFirstWord.length; i++) {
    if (!arrSecondWord.includes(arrFirstWord[i])) {
      couter++;
    }
  }

  res.status(200).json({ result: couter === 0 });
};

module.exports = {
  isContainLetter,
};
