# API

This is a Rest API with [Node.js](https://nodejs.org/en/) using [Express](https://expressjs.com/) framework.

## Getting Started

Get All Dependency

```bash
npm install
# or
yarn install
```

Run the development server:

```bash
npm run dev
# or
yarn dev
```

By default the server will run on `http://localhost:3000`.

## ENDPOINT

### FOOTBALL

#### [POST] `/football/recordgame`
This endpoint is used to add football game records. Parameter is clubhomename, clubawayname ,score (string with semicolon).

Request Body (JSON)
```json
{
    "clubhomename": "", // ex: Persija
    "clubawayname": "", // ex: PSM Makassar
    "score": "" // ex: "1 : 2"
}
```

Response Expectation
```json
{
    "message": "Record Game Stored Successfully!"
}
```

Results <br />
<img src="https://i.postimg.cc/NFQvXTbc/Screenshot-2023-02-14-at-09-31-13.png" width="90%">


#### [GET] `/football/leaguestanding`
This endpoint is used to get the overall data from the temporary standing of a football match. No parameter and return clubname and current points.

Response Expectation
```json
{
    "message": "League Standing Retrieved Successfully!",
    "data": [...]
}
```

Results <br />
<img src="https://i.postimg.cc/g0hCB4Tp/Screenshot-2023-02-14-at-09-37-08.png" width="90%">

#### [GET] `/football/rank`
This endpoint is used to get data based on the `clubname` parameter entered in the _params query_. Parameter is clubname and return name of club and current ranking/standing.

Request Query (JSON)
```json
{
    "clubname": "", // Persija
}
```

Response Expectation
```json
{
    "message": "Rank Retrieved Successfully!",
    "data": [...]
}
```

Results <br />
<img src="https://i.postimg.cc/dtvG71Yj/Screenshot-2023-02-14-at-09-49-12.png" width="90%">

### CONTAIN LETTER

#### [POST] `/is-contain-letters`
This endpoint to implement containLetters function that, given two words, returns true if all the letters of the first word are contained in the second.

Request Body (JSON)
```json
{
    "first_word": "", // ex: cat
    "second_word": "", // ex: antarctica
}
```

Response Expectation
```json
{
    "result": bool // true or false
}
```

Result <br />
<img src="https://i.postimg.cc/W3GhjGhL/Screenshot-2023-02-14-at-10-02-50.png" width="90%">
